package imagesUser

import (
	"encoding/base64"
	"image"
	"image/png"
	"os"
	//   "io"
	"bytes"
	"github.com/samwho/streamdeck"
	"strings"
)

type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

func GetImageFromDataUrl(dataUrl string) (image.Image, error) {
	if !strings.HasPrefix(dataUrl, "data:image/png;base64,") {
		return nil, &errorString{"Not a valid base64 png data url"}
	}
	unbased, errB64 := base64.StdEncoding.DecodeString(dataUrl[22:])
	if errB64 != nil {
		return nil, errB64
	}
	pngI, errPng := png.Decode(bytes.NewReader(unbased))
	if errPng != nil {
		return nil, errPng
	}
	return pngI, nil
}

func GetImageAsDataUrl(file image.Image) (string, error) {
	dataUrl, errB64 := streamdeck.Image(file)

	if errB64 == nil {
		return dataUrl, nil
	}

	return "", errB64
}

func LoadImg(filename string) (image.Image, error) {
	infile, err := os.Open(filename)
	if err != nil {
		// replace this with real error handling
		return nil, err
	}
	defer infile.Close()

	img, _, err := image.Decode(infile)
	if err != nil {
		// replace this with real error handling
		return nil, err
	}

	return img, nil
}
