package actionTime

import (
	"context"
	"encoding/json"
	// 	"fmt"
	// 	"image"
	// 	"image/color"
	"time"

	"github.com/samwho/streamdeck"
	sdcontext "github.com/samwho/streamdeck/context"

	"../../osc/listener"
	// 	"../../osc/client"
)

type TimeSettings struct {
	//   Track int `json:"track,string" min:"1" max:"99"`
	//   OnKeyPress string `json:"onkeypress"`
	//   OnKeyRelease string `json:"onkeyrelease"`
	Color string `json:"color"`
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.time")
	settings := make(map[string]*TimeSettings)

	//   action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	//     p := streamdeck.DidReceiveSettingsPayload{}
	//     sr := &TimeSettings{}
	//     s, ok := settings[event.Context]
	//     if !ok {
	//       s = &TimeSettings{}
	//       settings[event.Context] = s
	//     }
	//
	//     if err := json.Unmarshal(event.Payload, &p); err == nil {
	//       err := json.Unmarshal(p.Settings, &sr)
	//       if err == nil {
	//         settings[event.Context] = sr
	//       }
	//     }
	//     return nil
	//   })

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := settings[event.Context]
		if !ok {
			s = &TimeSettings{}
			settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		return client.SetTitle(ctx, oscListener.ReaperState.TimeString, streamdeck.HardwareAndSoftware)
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := settings[event.Context]
		return client.SetSettings(ctx, s)
	})
	//
	// 	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	// 		s, ok := settings[event.Context]
	// 		if !ok {
	// 			return fmt.Errorf("couldn't find settings for context %v", event.Context)
	// 		}
	//
	//     switch s.OnKeyPress {
	//       case "mute":
	//         oscClient.TrackMute(s.Track)
	//       case "unmute":
	//         oscClient.TrackUnmute(s.Track)
	//     }
	//
	//     return nil
	// 	})
	//
	// 	action.RegisterHandler(streamdeck.KeyUp, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	// 		s, ok := settings[event.Context]
	// 		if !ok {
	// 			return fmt.Errorf("couldn't find settings for context %v", event.Context)
	// 		}
	//
	//     switch s.OnKeyRelease {
	//       case "mute":
	//         oscClient.TrackMute(s.Track)
	//       case "unmute":
	//         oscClient.TrackUnmute(s.Track)
	//     }
	//
	//     return nil
	// 	})

	go func() {
		for range time.Tick(time.Second / 10) {
			for ctxStr := range settings {
				ctx := context.Background()
				ctx = sdcontext.WithContext(ctx, ctxStr)
				//           s, _ := settings[ctxStr]

				client.SetTitle(ctx, oscListener.ReaperState.TimeString, streamdeck.HardwareAndSoftware)

			}
		}
	}()
}
