package actionSoundboard

import (
	"context"
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"strconv"
	"strings"
	"time"

	"github.com/samwho/streamdeck"
	sdcontext "github.com/samwho/streamdeck/context"

	"../../colors/decoder"
	"../../images/backgrounds"
	"../../images/combine"
	"../../images/overlays"
	"../../images/user"
	"../../osc/client"
	"../../osc/listener"
)

type SoundboardSettings struct {
	// general
	Player int `json:"player,string" min:"1" max:"24"`

	// not playing
	OnKeyPress        string `json:"onKeyPress"`
	Display           string `json:"display"`
	Color             string `json:"color"`
	Overlay           string `json:"overlay"`
	UserOverlayPath   string `json:"useroverlay"`
	UserOverlayBase64 string `json:"useroverlay_base64"`

	// playing
	OnKeyPressWhilePlaying   string `json:"onKeyPressWhilePlaying"`
	OnKeyReleaseWhilePlaying string `json:"onKeyReleaseWhilePlaying"`
	DisplayWhilePlaying      string `json:"displayWhilePlaying"`
	ColorWhilePlaying        string `json:"colorWhilePlaying"`

	// done
	DisplayDone string `json:"displayDone"`
	ColorDone   string `json:"colorDone"`
}

type EventPayload struct {
	SDPICollection SDPICollection `json:"sdpi_collection"`
}

type SDPICollection struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type SoundboardCacheType struct {
	FilledPx         int
	Blink            bool
	Images           [73]string // 0-72 filled px
	ImagesBlink      [73]string
	UserOverlayImage image.Image
}

var Settings map[string]*SoundboardSettings
var Cache map[string]*SoundboardCacheType

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.soundboard")
	Settings = make(map[string]*SoundboardSettings)
	Cache = make(map[string]*SoundboardCacheType)

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		client.LogMessage("DidReceiveSettings")
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &SoundboardSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &SoundboardSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}

		Cache[event.Context] = &SoundboardCacheType{}

		client.LogMessage(event.Context)
		client.LogMessage("Build image cache NEW")

		if s.UserOverlayPath != "" {
			client.LogMessage("User defined overlay: " + s.UserOverlayPath)

			img, errImg := imagesUser.LoadImg(s.UserOverlayPath)

			Cache[event.Context].UserOverlayImage = nil
			Settings[event.Context].UserOverlayBase64 = ""
			Settings[event.Context].UserOverlayPath = ""
			s.UserOverlayPath = ""
			if errImg == nil {
				dataUrl, errB64 := imagesUser.GetImageAsDataUrl(img)
				if errB64 == nil {
					Settings[event.Context].UserOverlayBase64 = dataUrl
					client.SetSettings(ctx, Settings[event.Context])
				}
			}
		}

		buildImageCache(event.Context)
		client.LogMessage("Image cache done")
		client.LogMessage("Set Image")
		client.SetImage(ctx, Cache[event.Context].Images[0], streamdeck.HardwareAndSoftware)
		client.LogMessage("Set Image done")

		b, jErr := json.Marshal(Settings[event.Context])
		if jErr == nil {
			client.LogMessage(string(b))
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		client.LogMessage("WillAppear")
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &SoundboardSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		if s.Player < 1 || s.Player > 24 {
			s.Player = 1
			Settings[event.Context] = s
		}

		Cache[event.Context] = &SoundboardCacheType{}

		if s.UserOverlayPath != "" {
			client.LogMessage("User defined overlay: " + s.UserOverlayPath)

			img, errImg := imagesUser.LoadImg(s.UserOverlayPath)

			Cache[event.Context].UserOverlayImage = nil
			Settings[event.Context].UserOverlayBase64 = ""
			Settings[event.Context].UserOverlayPath = ""
			s.UserOverlayPath = ""
			if errImg == nil {
				dataUrl, errB64 := imagesUser.GetImageAsDataUrl(img)
				if errB64 == nil {
					Settings[event.Context].UserOverlayBase64 = dataUrl
					client.SetSettings(ctx, Settings[event.Context])
				}
			}
		}

		buildImageCache(event.Context)
		client.SetImage(ctx, Cache[event.Context].Images[0], streamdeck.HardwareAndSoftware)

		return client.SetTitle(ctx, getKeyTitle(s), streamdeck.HardwareAndSoftware)
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if !ok {
			return fmt.Errorf("couldn't find settings for context %v", event.Context)
		}

		oscClient.SoundboardPlay(s.Player)

		return client.SetTitle(ctx, getKeyTitle(s), streamdeck.HardwareAndSoftware)
	})

	go func() {
		tickCounter := 0
		for range time.Tick(time.Second / 10) {
			if tickCounter > 9 {
				tickCounter = 0
			}

			for ctxStr := range Settings {
				ctx := context.Background()
				ctx = sdcontext.WithContext(ctx, ctxStr)
				s, _ := Settings[ctxStr]

				blink := false

				remaining := oscListener.SbStates[s.Player-1].Remaining
				if strings.HasPrefix(remaining, "-0:00:") {
					remainingParts := strings.Split(remaining, ":")
					if len(remainingParts) == 3 {
						secsStr := remainingParts[2]
						secs, err := strconv.Atoi(secsStr)

						if err == nil {
							if secs < 5 && (tickCounter == 0 || tickCounter == 1 || tickCounter == 5 || tickCounter == 6) {
								// fast blinking
								blink = true
							} else if secs < 10 && (tickCounter == 0) {
								// slow blinking
								blink = true
							}
						}

					}
				}

				filledPx := int(72 * oscListener.SbStates[s.Player-1].Progress)

				if Cache[ctxStr].FilledPx != filledPx || Cache[ctxStr].Blink != blink || true {
					if blink {
						imgStr := Cache[ctxStr].ImagesBlink[filledPx]
						if imgStr != "" {
							client.SetImage(ctx, imgStr, streamdeck.HardwareAndSoftware)
						}
					} else {
						imgStr := Cache[ctxStr].Images[filledPx]
						if imgStr != "" {
							client.SetImage(ctx, imgStr, streamdeck.HardwareAndSoftware)
						}
					}
					Cache[ctxStr].FilledPx = filledPx
					Cache[ctxStr].Blink = blink
				}
				client.SetTitle(ctx, getKeyTitle(s), streamdeck.HardwareAndSoftware)

			}

			tickCounter++
		}
	}()
}

func getKeyTitle(s *SoundboardSettings) string {
	displaySetting := s.Display
	state := oscListener.SbStates[s.Player-1]

	if state.Playing == true {
		displaySetting = s.DisplayWhilePlaying
	} else if state.Done == true {
		displaySetting = s.DisplayDone
	}

	title := ""
	name := "Player " + strconv.Itoa(s.Player)

	if len(state.Title) > 0 {
		name = state.Title
	}

	if displaySetting == "name" {
		title = name
	}
	if displaySetting == "name_time" || displaySetting == "name_remaining" {
		title = name + "\n"
	}
	if displaySetting == "time" || displaySetting == "name_time" {
		title = title + state.Time
	}
	if displaySetting == "remaining" || displaySetting == "name_remaining" {
		title = title + state.Remaining
	}

	return title
}

func buildImageCache(ctxStr string) {
	var imgOverlay image.Image

	s := Settings[ctxStr]

	if s.Overlay == "_userdefined" {
		if s.UserOverlayBase64 != "" {
			img, errImg := imagesUser.GetImageFromDataUrl(s.UserOverlayBase64)
			if errImg == nil {
				imgOverlay = img
				Cache[ctxStr].UserOverlayImage = img
			}
		}
	} else if s.Overlay != "" && s.Overlay != "_none" {
		imgOverlay, _ = imagesOverlays.Overlays[s.Overlay]
	}

	colorNormal, _ := colorsDecoder.RGBHexToRGBA(s.Color, 255, color.RGBA{0, 255, 0, 128})
	colorBg := colorsDecoder.DarkenCol(colorNormal, 0.3)
	colorBlink := colorsDecoder.LightenCol(colorNormal, 0.3)

	for i := 0; i <= 72; i++ {
		//     imgNormalBg := imagesCombine.CombineTwo(imagesBackgrounds.Black, imagesBackgrounds.ProgressHorizontalLeftRightFullHeight(i, colorNormal, colorBg))
		//     imgBlinkBg := imagesCombine.CombineTwo(imagesBackgrounds.Black, imagesBackgrounds.ProgressHorizontalLeftRightFullHeight(i, colorBlink, colorBg))
		imgNormalBg := imagesBackgrounds.ProgressHorizontalLeftRightFullHeight(i, colorNormal, colorBg)
		imgBlinkBg := imagesBackgrounds.ProgressHorizontalLeftRightFullHeight(i, colorBlink, colorBg)

		if imgOverlay != nil {
			// with overlay
			imgNormal, errNormal := streamdeck.Image(imagesCombine.CombineTwo(imgNormalBg, imgOverlay))
			imgBlink, errBlink := streamdeck.Image(imagesCombine.CombineTwo(imgBlinkBg, imgOverlay))

			if errNormal == nil {
				Cache[ctxStr].Images[i] = imgNormal
			} else {
				Cache[ctxStr].Images[i] = ""
			}
			if errBlink == nil {
				Cache[ctxStr].ImagesBlink[i] = imgBlink
			} else {
				Cache[ctxStr].ImagesBlink[i] = ""
			}
		} else {
			// without overlay
			imgNormal, errNormal := streamdeck.Image(imgNormalBg)
			imgBlink, errBlink := streamdeck.Image(imgBlinkBg)

			if errNormal == nil {
				Cache[ctxStr].Images[i] = imgNormal
			} else {
				Cache[ctxStr].Images[i] = ""
			}
			if errBlink == nil {
				Cache[ctxStr].ImagesBlink[i] = imgBlink
			} else {
				Cache[ctxStr].ImagesBlink[i] = ""
			}
		}
	}
	// hack?
	Cache[ctxStr].FilledPx = 73
}
