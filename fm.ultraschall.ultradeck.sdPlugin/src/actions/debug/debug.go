package actionDebug

import (
	"context"
	"encoding/json"
	"strconv"
	"strings"

	"github.com/samwho/streamdeck"
)

type Settings struct {
	Track int `json:"track"`
}

type Payload struct {
	SDPICollection SDPICollection `json:"sdpi_collection"`
}

type SDPICollection struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.debug")
	settings := make(map[string]*Settings)

	//   action.RegisterHandler(streamdeck.SendToPlugin, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	//     var pl Payload
	//     if err := json.Unmarshal(event.Payload, &pl); err == nil {
	//       client.SetTitle(ctx, pl.SDPICollection.Key, streamdeck.HardwareAndSoftware)
	//     }
	//     s, ok := settings[event.Context]
	//     if !ok {
	//       s = &Settings{}
	//       settings[event.Context] = s
	//     }
	//     return json.Unmarshal(event.Payload, &SDPICollection{})
	//   })

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		if err := json.Unmarshal(event.Payload, &p); err == nil {
			j, err := json.Marshal(&p.Settings)
			if err == nil {
				client.SetTitle(ctx, strings.ReplaceAll(string(j), "\",", "\",\n"), streamdeck.HardwareAndSoftware)
			} else {
				client.SetTitle(ctx, "error 2", streamdeck.HardwareAndSoftware)
			}
		} else {
			client.SetTitle(ctx, "error 1", streamdeck.HardwareAndSoftware)
		}
		s, ok := settings[event.Context]
		if !ok {
			s = &Settings{}
			settings[event.Context] = s
		}
		return json.Unmarshal(event.Payload, &SDPICollection{})
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := settings[event.Context]
		if !ok {
			s = &Settings{}
			settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		return client.SetTitle(ctx, strconv.Itoa(s.Track), streamdeck.HardwareAndSoftware)
	})

	// 	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	// 		s, _ := settings[event.Context]
	// 		s.Counter = 0
	// 		return client.SetSettings(ctx, s)
	// 	})

	// 	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
	// 		s, ok := settings[event.Context]
	// 		if !ok {
	// 			return fmt.Errorf("couldn't find settings for context %v", event.Context)
	// 		}
	//
	// 		s.Counter++
	// 		if err := client.SetSettings(ctx, s); err != nil {
	// 			return err
	// 		}
	//
	// 		return client.SetTitle(ctx, strconv.Itoa(s.Counter), streamdeck.HardwareAndSoftware)
	// 	})
}
