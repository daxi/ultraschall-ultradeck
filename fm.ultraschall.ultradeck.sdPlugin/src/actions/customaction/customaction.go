package actionCustomAction

import (
	"context"
	"encoding/json"
	"strconv"

	"github.com/samwho/streamdeck"

	"../../osc/client"
)

type ActionSettings struct {
	//   Track int `json:"track,string" min:"1" max:"99"`
	//   OnKeyPress string `json:"onkeypress"`
	//   OnKeyRelease string `json:"onkeyrelease"`
	OnPress   string `json:"onpress"`
	OnRelease string `json:"onrelease"`
}

var Settings map[string]*ActionSettings

func init() {
	Settings = make(map[string]*ActionSettings)
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.customaction")

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &ActionSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &ActionSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &ActionSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if ok {
			if s.OnPress != "" {
				actionId, err := strconv.Atoi(s.OnPress)
				if err == nil {
					oscClient.ExecActionById(actionId)
				} else {
					oscClient.ExecActionByName(s.OnPress)
				}
			}
		}

		return nil
	})

	action.RegisterHandler(streamdeck.KeyUp, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if ok {
			if s.OnRelease != "" {
				actionId, err := strconv.Atoi(s.OnRelease)
				if err == nil {
					oscClient.ExecActionById(actionId)
				} else {
					oscClient.ExecActionByName(s.OnRelease)
				}
			}
		}

		return nil
	})
}
