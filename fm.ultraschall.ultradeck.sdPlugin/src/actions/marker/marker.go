package actionMarker

import (
	"context"
	"encoding/json"
	"image"

	"github.com/samwho/streamdeck"

	"../../images/backgrounds"
	"../../images/full"
	"../../osc/client"
)

type MatrixSettings struct {
	Action      string `json:"action"`
	ActiveImage string
}

var Settings map[string]*MatrixSettings
var CurrentImage map[string]string

func init() {
	Settings = make(map[string]*MatrixSettings)
	CurrentImage = make(map[string]string)
}

func SetupAction(client *streamdeck.Client) {
	action := client.Action("fm.ultraschall.ultradeck.marker")

	action.RegisterHandler(streamdeck.DidReceiveSettings, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.DidReceiveSettingsPayload{}
		sr := &MatrixSettings{}
		s, ok := Settings[event.Context]
		if !ok {
			s = &MatrixSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(event.Payload, &p); err == nil {
			err := json.Unmarshal(p.Settings, &sr)
			if err == nil {
				Settings[event.Context] = sr
			}
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}
		client.SetTitle(ctx, s.Action, streamdeck.HardwareAndSoftware)

		return nil
	})

	action.RegisterHandler(streamdeck.WillAppear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		p := streamdeck.WillAppearPayload{}
		if err := json.Unmarshal(event.Payload, &p); err != nil {
			return err
		}

		s, ok := Settings[event.Context]
		if !ok {
			s = &MatrixSettings{}
			Settings[event.Context] = s
		}

		if err := json.Unmarshal(p.Settings, s); err != nil {
			return err
		}

		CurrentImage[event.Context] = ""
		img := getTypeImage(event.Context)
		if img != nil {
			Settings[event.Context] = s
			bg, err1 := streamdeck.Image(img)
			if err1 == nil {
				client.SetImage(ctx, bg, streamdeck.HardwareAndSoftware)
			}
		}

		return nil
	})

	action.RegisterHandler(streamdeck.WillDisappear, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, _ := Settings[event.Context]
		CurrentImage[event.Context] = ""
		return client.SetSettings(ctx, s)
	})

	action.RegisterHandler(streamdeck.KeyDown, func(ctx context.Context, client *streamdeck.Client, event streamdeck.Event) error {
		s, ok := Settings[event.Context]
		if ok {
			switch s.Action {
			case "chapter":
				oscClient.SetChapterMarker()
			case "chapter_backintime":
				oscClient.SetChapterMarkerBackInTime()
			case "edit":
				oscClient.SetEditMarker()
			case "edit_playpos":
				oscClient.SetEditMarkerAtPlayPos()
			case "delete_last":
				oscClient.DeleteLastMarker()
			}
		}

		return nil
	})
}

func getTypeImage(ctxStr string) image.Image {
	s := Settings[ctxStr]
	cI := CurrentImage[ctxStr]

	switch s.Action {
	case "chapter":
		if cI != "chapter" {
			CurrentImage[ctxStr] = "chapter"
			return imagesFull.MarkerChapter
		}
	case "chapter_backintime":
		if cI != "chapter_backintime" {
			CurrentImage[ctxStr] = "chapter_backintime"
			return imagesFull.MarkerChapterBackInTime
		}
	case "edit", "edit_playpos":
		if cI != "edit" {
			CurrentImage[ctxStr] = "edit"
			return imagesFull.MarkerEdit
		}
	default:
		if cI != "black" {
			CurrentImage[ctxStr] = "black"
			return imagesBackgrounds.Black
		}
	}

	return nil
}
