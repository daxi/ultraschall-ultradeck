package main

import (
	"context"
	"os"

	"github.com/samwho/streamdeck"

	"./actions/counter"
	"./actions/customaction"
	"./actions/debug"
	"./actions/marker"
	"./actions/routing"
	"./actions/soundboard"
	"./actions/time"
	"./actions/track"
	"./actions/transport"
	"./actions/view"
	"./osc/listener"
	"./webapi"
)

func main() {
	go oscListener.Server(false).ListenAndServe()
	go webApi.PollWebAPI()
	ctx := context.Background()
	run(ctx)
}

func run(ctx context.Context) error {
	params, err := streamdeck.ParseRegistrationParams(os.Args)
	if err != nil {
		return err
	}

	client := streamdeck.NewClient(ctx, params)

	actionCounter.SetupAction(client)
	actionTrack.SetupAction(client)
	actionSoundboard.SetupAction(client)
	actionTime.SetupAction(client)
	actionTransport.SetupAction(client)
	actionRouting.SetupAction(client)
	actionCustomAction.SetupAction(client)
	actionMarker.SetupAction(client)
	actionView.SetupAction(client)

	actionDebug.SetupAction(client)

	return client.Run()
}
